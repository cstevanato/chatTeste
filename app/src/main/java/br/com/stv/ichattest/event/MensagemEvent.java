package br.com.stv.ichattest.event;

import br.com.stv.ichattest.modelo.Mensagem;

public class MensagemEvent {

    public final Mensagem mensagem;

    public MensagemEvent(Mensagem mensagem) {
        this.mensagem = mensagem;
    }

}
