package br.com.stv.ichattest.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;

import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import br.com.stv.ichattest.R;
import br.com.stv.ichattest.adapter.MensagemAdapter;
import br.com.stv.ichattest.app.ChatApplication;
import br.com.stv.ichattest.callback.EnviarMensagemCallback;
import br.com.stv.ichattest.callback.OuverMensagensCallBack;
import br.com.stv.ichattest.component.ChatComponent;
import br.com.stv.ichattest.event.MensagemEvent;
import br.com.stv.ichattest.modelo.Mensagem;
import br.com.stv.ichattest.service.ChatServiceInterface;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;

public class MainActivity extends AppCompatActivity {

    private int idDoCliente = 1;
    @BindView(R.id.txt_texto)
    EditText editText;

    @BindView(R.id.btn_enviar)
    Button button;

    @BindView(R.id.lv_mensagens)
    ListView listaMensagens;

    @BindView(R.id.iv_avatar_usuario)
    ImageView avatar;

    private List<Mensagem> mensagens;

    @Inject
    ChatServiceInterface chatService;

    @Inject
    Picasso picasso;

    @Inject
    EventBus eventBus;

    private ChatComponent component;

    //    private BroadcastReceiver reciver = new BroadcastReceiver() {
//        @Override
//        public void onReceive(Context context, Intent intent) {
//            Mensagem mensagem = (Mensagem) intent.getSerializableExtra("mensagem");
//            colocaNaLista(mensagem);
//        }
//    };


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putSerializable("mensagens", (ArrayList<Mensagem>) mensagens);
        //outState.putParcelableArrayList("mensagens", mensagens);
    }

    @Override
    protected void onStop() {
        super.onStop();

        eventBus.unregister(this);
//        LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(this);
//        localBroadcastManager.unregisterReceiver(reciver);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

//        LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(this);
//        localBroadcastManager.registerReceiver(reciver, new IntentFilter("nova_mensagem"));


        ChatApplication app = (ChatApplication) getApplication();
        component = app.getComponet();
        component.inject(this);

        eventBus.register(this);
        picasso.get().load("https://api.adorable.io/avatars/285/" + idDoCliente + ".png").into(avatar);

        if (savedInstanceState != null) {
            mensagens = (List<Mensagem>) savedInstanceState.getSerializable("mensagens");
        } else {
            mensagens = new ArrayList<>();
        }

        MensagemAdapter adapter = new MensagemAdapter(idDoCliente, mensagens, this);
        listaMensagens.setAdapter(adapter);

        ouvirMensagem(null);

    }

    @OnClick(R.id.btn_enviar)
    public void enviarMensagem() {
        chatService.enviar(new Mensagem(idDoCliente, editText.getText().toString()))
                .enqueue(new EnviarMensagemCallback());
        editText.getText().clear();
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        if (inputMethodManager != null) {
            inputMethodManager.hideSoftInputFromWindow(editText.getWindowToken(), 0);
        }
    }

    @Subscribe
    public void colocaNaLista(MensagemEvent mensagemEvent) {
        mensagens.add(mensagemEvent.mensagem);

        MensagemAdapter adapter = new MensagemAdapter(idDoCliente, mensagens, this);
        listaMensagens.setAdapter(adapter);


    }

    @Subscribe
    public void ouvirMensagem(MensagemEvent mensagemEvent) {
        Call<Mensagem> call = chatService.ouvirMensagem();
        call.enqueue(new OuverMensagensCallBack(this, eventBus));
    }
}
