package br.com.stv.ichattest.callback;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.text.NoCopySpan;

import org.greenrobot.eventbus.EventBus;

import javax.inject.Inject;

import br.com.stv.ichattest.activity.MainActivity;
import br.com.stv.ichattest.event.MensagemEvent;
import br.com.stv.ichattest.modelo.Mensagem;
import retrofit2.Call;
import retrofit2.Response;

public class OuverMensagensCallBack implements retrofit2.Callback<Mensagem> {

    private final Context context;

    private EventBus eventBus;

    public OuverMensagensCallBack(Context context, EventBus eventBus) {
        this.context = context;
        this.eventBus = eventBus;
    }

    @Override
    public void onResponse(Call<Mensagem> call, Response<Mensagem> response) {
        if (response.isSuccessful()) {
            Mensagem mensagem = response.body();
            //activity.colocaNaLista(mensagem);

//            Intent intent =  new Intent("nova_mensagem");
//            intent.putExtra("mensagem", mensagem);
//            LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(context);
//            localBroadcastManager.sendBroadcast(intent);

            eventBus.post(new MensagemEvent(mensagem));
        }
    }

    @Override
    public void onFailure(Call<Mensagem> call, Throwable t) {
        //activity.ouvirMensagem();
    }
}
