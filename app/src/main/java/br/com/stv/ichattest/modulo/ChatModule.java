package br.com.stv.ichattest.modulo;

import android.app.Application;
import android.util.EventLog;

import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;

import br.com.stv.ichattest.service.ChatServiceInterface;
import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class ChatModule {

    private Application app;

    public ChatModule(Application app) {
        this.app = app;
    }


    @Provides
    public ChatServiceInterface getChatService() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://192.168.0.106:8080/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ChatServiceInterface chatService = retrofit.create(ChatServiceInterface.class);
        return chatService;
    }

    @Provides
    public Picasso getPicasso() {
        Picasso build = new Picasso.Builder(app).build();
        return build;
    }

    @Provides
    public EventBus getEventBus() {
        return EventBus.builder().build();
    }
}
