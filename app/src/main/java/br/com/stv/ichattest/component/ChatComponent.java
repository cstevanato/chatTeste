package br.com.stv.ichattest.component;

import br.com.stv.ichattest.activity.MainActivity;
import br.com.stv.ichattest.adapter.MensagemAdapter;
import br.com.stv.ichattest.modulo.ChatModule;
import dagger.Component;

@Component(modules = ChatModule.class)
public interface ChatComponent {

    void inject(MainActivity activity);
    void inject(MensagemAdapter mensagemAdapter);
}
