package br.com.stv.ichattest.modelo;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Mensagem implements Serializable {
    @SerializedName("text")
    private String texto;
    private int id;

    public Mensagem(int id, String mensagem) {
        this.texto = mensagem;
        this.id = id;
    }

    public String getTexto() {
        return texto;
    }

    public int getId() {
        return id;
    }
}
