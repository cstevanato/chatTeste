package br.com.stv.ichattest.app;

import android.app.Application;

import br.com.stv.ichattest.component.ChatComponent;
import br.com.stv.ichattest.component.DaggerChatComponent;
import br.com.stv.ichattest.modulo.ChatModule;

public class ChatApplication extends Application {

    private ChatComponent component;

    @Override
    public void onCreate() {
        super.onCreate();
        component = DaggerChatComponent.builder()
                .chatModule(new ChatModule(this))
                .build();

    }

    public ChatComponent getComponet() {
        return component;
    }
}
